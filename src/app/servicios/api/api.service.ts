import { Injectable } from '@angular/core';
import { LoginI } from '../../modelos/login.interface'
import { ResponseI } from '../../modelos/Response.interface'
import { TaskI } from '../../modelos/task.interface';
import { UsuarioI } from '../../modelos/usuario.interface'
import { mensajeI } from '../../modelos/mensaje.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  url: string = "http://127.0.0.1:4003/";
  // url : string = "http://jsonplaceholder.typicode.com/"
  constructor(private http: HttpClient) { }

  loginByEmail(form: LoginI): Observable<ResponseI> {
    let direccion = this.url + 'usuarios/auth'
    return this.http.post<ResponseI>(direccion, form)
  }

  getOneUser():Observable<UsuarioI> {
    let path = this.url + 'usuarios/me'
    return this.http.get<UsuarioI>(path)
  }

  getTask(id: string) {
    let path = this.url + `todos/${id}`
    return this.http.get<TaskI>(path)
  }

  getHola() {
    let path = this.url
    return this.http.get<mensajeI>(path)
  }

}
