
export interface UsuarioI {
    iId: number;
    sIdUsuario: string;
    iId_Empleado: number;
    iId_Departamento: number;
    iId_Grupo: number;
    iIdProveedor: number;
    iIdCliente: number;
    iIdUnidadConductor: number;
    sNombre: string;
    sMail: string;
    sTelefono: string;
}