export interface TaskI{
    userId: string;
    id: string;
    title:string;
    completed: boolean;

}