

export interface ResponseI {
    /* Aqui cachamos los status de respuestas 
    * ok or error 
    */
    status: string;
    result: any;
    error: string;
    ok: boolean;
    sIdUsuario: string;
    data: JSON;
}