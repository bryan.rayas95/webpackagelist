import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../servicios/api/api.service'
import { LoginI } from '../../modelos/login.interface'
import { ResponseI } from '../../modelos/Response.interface';
import { Router } from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    identificador: new FormControl('', Validators.required),
    clave: new FormControl('', Validators.required)
  })

  constructor(private api: ApiService, private router: Router) { }

  errorStatus: boolean = false;
  errorMsj: string = "";

  ngOnInit(): void {
    this.chechLocalStorage()
  }

  chechLocalStorage() {
    if (localStorage.getItem('token')) {
      this.router.navigate(['dashboard']);
    }
  }

  onLogin(form: LoginI) {
    this.api.loginByEmail(form).subscribe(data => {

      let dataResponse: ResponseI = data;

      if (data) {
        console.log('----', dataResponse.data)
        localStorage.setItem('token', dataResponse.sIdUsuario)
        this.router.navigate(['dashboard'])
      } else {
        console.log('***** ', dataResponse.error)
        this.errorStatus = true;
        this.errorMsj = dataResponse.error;
      }
    });


  }
}
