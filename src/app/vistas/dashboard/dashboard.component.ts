import { Component, OnInit } from '@angular/core';
import { TaskI } from '../../modelos/task.interface';
import { ApiService } from '../../servicios/api/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getOneUser().subscribe(todos => {
      console.log(todos)
    });
  }

  getAllTasks() {
    this.api.getOneUser().subscribe(todos => {
      console.log(todos)
    });
  }

}
